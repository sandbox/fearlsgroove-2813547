Moki Environment is a helper module to provide some conventions for action taken when switching environments using the environment module. It supports automatically making the desired set of enabled modules match, setting variables and solr environments.

The module allows you to define default states for modules and variables. Modules can be supplemented with a set of environment-specific modules, and variables can be overridden with an environment specific set.

Installation: the typical download and install process.

Usage:

Create files in your sites configuration directory (i.e. sites/default or sites/<your site name>) to define the settings you want. These files are just normal PHP files that are automatically included if available when switching environments. The files use specific variables to collect the information required, but there are no restrictions on what you might include here.

Naming conventions available are:

modules_baseline.inc: the default set of modules that are always enabled whenever switching environments.
  * See modules_baseline.inc.example
modules_<environment code>.inc: included if present after modules baseline but before attempting to make the modules list match the desired state. The idea is that one might add to the list of modules here.
  * See modules_env.inc.example
variables_default.inc: Define a default list of variables.
  * See variables_xxx.inc.example
variables_<environment code>.inc: included *instead* of the variables default, if the matching environment file is present. Note that unlike modules, it doesn't include the default set; you must define all the variables you require.
  * See variables_xxx.inc.example
solr_default.inc/solr_<environment>.inc: allows you to define settings for the solr environment.
  * see solr_xxx.inc.example